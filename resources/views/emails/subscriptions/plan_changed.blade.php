@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

        <h4 class="secondary"><strong>Subscription Update</strong></h4>
        <p>Your subscription has changed</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop