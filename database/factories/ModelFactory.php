<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Account::class, function ($faker) {
    return [
        'id' =>  $faker->numberBetween($min = 1000000000, $max = 9999999999),
        'name' => $faker->company,
        'description' => $faker->catchPhrase,
    ];
});

$factory->define(App\Models\Stat::class, function ($faker) {
    $days = 30;
    $startDate = $faker->dateTimeBetween('-' . $days . ' days', '-' . ($days - 1) . ' days');
    return [
        'account_id' => App\Models\Account::all()->random()->id,
        'cost' => $faker->numberBetween($min = 10, $max = 100),
        'avg_cpc' => $faker->randomFloat($nbMaxDecimals = 2, $min = 2, $max = 10),
        'ctr' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 5),
        'impressions' => $faker->numberBetween($min = 50, $max = 1000),
        'clicks' => $faker->numberBetween($min = 25, $max = 500),
        'avg_position' => $faker->randomFloat($nbMaxDecimals = 1, $min = 1, $max = 5),
        'created_at' => $startDate,
    ];
    $days = $days - 1;
});