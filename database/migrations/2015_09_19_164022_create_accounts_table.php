<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('date_time_zone')->nullable();
            $table->string('descriptive_name')->nullable();
            $table->string('company_name')->nullable();
            $table->string('description')->nullable();
            $table->boolean('can_manage')->default(false);
            $table->boolean('is_managed')->default(false);
            $table->boolean('test_account')->default(false);            
            $table->boolean('active')->default(false);
            $table->bigInteger('activated_by')->unsigned()->nullable()->default(null);
            $table->timestamp('activated_at')->nullable()->default(null);
            $table->tinyInteger('update_tick')->unsigned()->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->primary('id');

            /*$table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('accounts', function (Blueprint $table) {
            $table->dropForeign('accounts_user_id_foreign');
        });*/

        Schema::drop('accounts');
    }
}
