<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('avg_cpc')->unsigned()->nullable();
            $table->float('avg_position')->nullable();
            $table->integer('clicks')->unsigned()->nullable();
            $table->float('conversion_rate')->nullable();
            $table->integer('conversions')->unsigned()->nullable();
            $table->integer('converted_clicks')->unsigned()->nullable();
            $table->bigInteger('cost')->unsigned()->nullable();
            $table->float('ctr')->nullable();
            $table->integer('impressions')->unsigned()->nullable();
            $table->string('conversion_type_name')->nullable();
            $table->float('search_impression_share')->nullable();            
            $table->date('date');
            $table->timestamps();

            $table->index('account_id');

            $table->foreign('account_id')
                    ->references('id')
                    ->on('accounts')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stats', function (Blueprint $table) {
            $table->dropForeign('stats_account_id_foreign');
        });
        
        Schema::drop('stats');
    }
}
