<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccountsManagers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts_managers', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('manager_id')->unsigned();

            $table->primary(['account_id', 'manager_id']);

            /*$table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounts_managers');
    }
}
