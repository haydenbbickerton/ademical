<?php

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('google_id');
            $table->string('name');
            $table->string('display_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email');
            $table->string('picture');
            $table->string('etag')->nullable();
            $table->string('gender')->nullable();
            $table->string('language')->nullable();

            // Connections for their adwords account...
            $table->bigInteger('account_id')->unsigned()->nullable();
            $table->string('access_token')->nullable();
            $table->string('refresh_token')->nullable();
            $table->mediumInteger('token_expires_in')->unsigned()->nullable();
            $table->integer('token_timestamp')->unsigned()->nullable();

            // Two-Factor Authentication Columns...
            $table->string('phone_country_code')->nullable();
            $table->string('phone_number')->nullable();
            $table->text('two_factor_options')->nullable();

            // Cashier Columns...
            $table->tinyInteger('stripe_active')->default(0);
            $table->string('stripe_id')->nullable();
            $table->string('stripe_subscription')->nullable();
            $table->string('stripe_plan', 100)->nullable();
            $table->string('last_four', 4)->nullable();
            $table->text('extra_billing_info')->nullable();
            $table->timestamp('trial_ends_at')->nullable();
            $table->timestamp('subscription_ends_at')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->unique('account_id');
            $table->unique('google_id');
            $table->unique('email');
        });

        // Give ourselves some breating room with IDs
        // 111,111,111
        $statement = "
                        ALTER TABLE users AUTO_INCREMENT = 111111111;
                    ";

        DB::unprepared($statement);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
