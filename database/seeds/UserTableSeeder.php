<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::truncate();

       // factory(App\Models\Stat::class, 30)->create();

        $faker = Faker::create();


        App\Models\Account::all()->each(function ($item, $key) use ($faker) {
            $user = App\Models\User::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->safeEmail,
                'account_id' => $item->id,
            ]);

            $item->user_id = $user->id;
            $item->save();
        });


    }
}
