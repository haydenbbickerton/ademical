<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class StatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Stat::truncate();

       // factory(App\Models\Stat::class, 30)->create();

$faker = Faker::create();


App\Models\Account::all()->each(function ($item, $key) use ($faker) {

    for ($days = 30; $days >= 0; $days--) {

        $newDate = Carbon::now();

    App\Models\Stat::create([
        'account_id' => $item->id,
        'cost' => $faker->numberBetween($min = 5, $max = 75),
        'avg_cpc' => $faker->randomFloat($nbMaxDecimals = 2, $min = 2, $max = 10),
        'ctr' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 5),
        'impressions' => $faker->numberBetween($min = 25, $max = 75),
        'clicks' => $faker->numberBetween($min = 25, $max = 75),
        'avg_position' => $faker->randomFloat($nbMaxDecimals = 1, $min = 1, $max = 5),
        'created_at' => $newDate->subDays($days),
            ]);
}
});



/*        for ($days = 30; $days >= 0; $days--) {

$startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-1 month', 'now')->getTimestamp());
$account_id = App\Models\Account::all()->random()->id;

for ($num = 15; $num >= 0; $num--) {
App\Models\Stat::create([
        'account_id' => $account_id,
        'cost' => $faker->numberBetween($min = 10, $max = 100),
        'avg_cpc' => $faker->randomFloat($nbMaxDecimals = 2, $min = 2, $max = 10),
        'ctr' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 5),
        'impressions' => $faker->numberBetween($min = 50, $max = 1000),
        'clicks' => $faker->numberBetween($min = 25, $max = 500),
        'avg_position' => $faker->randomFloat($nbMaxDecimals = 1, $min = 1, $max = 5),
        'created_at' => $faker->dateTimeBetween('-' . $days . ' days', '-' . ($days - 1) . ' days'),
            ]);
}

            }*/

    }
}
