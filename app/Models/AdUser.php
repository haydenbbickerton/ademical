<?php

namespace App\Models;

use Config;
use AdWordsUser;
use Selector;

use Prettus\Repository\Contracts\Presentable;
use Prettus\Repository\Traits\PresentableTrait;

class AdUser extends AdWordsUser implements Presentable
{

    use PresentableTrait;

}
