<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Stat extends Model implements Transformable
{

    use TransformableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stats';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['updated_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'avg_cost' => 'integer',
        'avg_cpc' => 'integer',
        'avg_position' => 'float',
        'clicks' => 'integer',
        'conversion_rate' => 'float',
        'conversions' => 'integer',
        'converted_clicks' => 'integer',
        'cost' => 'integer',
        'ctr' => 'float',
        'impressions' => 'integer',
        'search_impression_share' => 'float',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
                        'created_at',
                        'updated_at'
                        ];

    /**
     * Get the account that owns the stat.
     */
    public function account()
    {
        return $this->belongsTo('App\Models\Account');
    }
}
