<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use App\Traits\ManageAccounts;

class Account extends Model implements Transformable
{

    use SoftDeletes, ManageAccounts, TransformableTrait;

    protected $primary_key = 'id';

    public $incrementing = false;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'id',
                            'user_id',
                            'descriptive_name',
                            'company_name',
                            'description',
                            'can_manage',
                            'is_manage',
                            'test_account',
                            'active',
                            'date_time_zone',
                            'activated_by',
                            'activated_at'
                            ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
                        'user_id',
                        'test_account'
                        ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
                        'id' => 'integer',
                        'user_id' => 'integer',
                        'can_manage' => 'boolean',
                        'is_managed' => 'boolean',
                        'test_account' => 'boolean',
                        'active' => 'boolean',
                        'activated_by' => 'integer',
                        'description' => 'string',
                        'descriptive_name' => 'string',
                        'company_name' => 'string'
                        ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
                        'activated_at',
                        'created_at',
                        'updated_at',
                        'deleted_at'
                        ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($account)
        {
            /*
             * Update Tick is the period during the day that we will update stats.
             *
             * 24 hours. 2 30-minute section per hour = 48
             */
            if (! isset($account->update_tick))
            {
                $account->update_tick = rand(1, 48);
            }
        });
    }

    /**
     * Get the user that owns the account.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the stats for the account.
     */
    public function stats()
    {
        return $this->hasMany('App\Models\Stat', 'account_id', 'id');
    }

    public function managers()
    {
        return $this->belongsToMany('App\Models\Account', 'accounts_managers', 'account_id', 'manager_id');
    }

    public function accounts()
    {
        return $this->belongsToMany('App\Models\Account', 'accounts_managers', 'manager_id', 'account_id');
    }

}
