<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

use Event;
use App\Events\UserWasCreated;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    BillableContract
{
    use Authenticatable, Authorizable, SoftDeletes, Billable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'google_id',
                            'access_token',
                            'refresh_token',
                            'token_expires_in',
                            'token_timestamp', 
                            'name', 
                            'first_name', 
                            'last_name', 
                            'email', 
                            'picture', 
                            'etag',
                            'gender',
                            'language',
                            'display_name'
                            ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
                        'google_id',
                        'access_token',
                        'refresh_token',
                        'email',
                        'etag',
                        'gender',
                        'language'
                        ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
                        'created_at', 
                        'updated_at', 
                        'deleted_at', 
                        'trial_ends_at', 
                        'subscription_ends_at'
                        ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
                        'account_id' => 'integer',
                        //'google_id' => 'string',
                        'name' => 'string',
                        'first_name' => 'string',
                        'last_name' => 'string',
                        'email' => 'string',
                        'stripe_active' => 'boolean',
                        'last_four' => 'integer',
                        ];

    public static function boot()
    {
        parent::boot();

        static::created(function($user)
        {
            // We fire off an event that will start the process
            // of fetching the users adwords info.
            Event::fire(new \App\Events\User\UserCreated($user));
        });
    }

    /**
     * Set the user's picture as a large size link.
     *
     * @param  string  $value
     * @return string
     */
    public function setPictureAttribute($value)
    {
        $this->attributes['picture'] = str_replace('sz=50', 'sz=200', $value);
    }

    /**
     * Get the adwords account for this user.
     */
    public function account()
    {
        return $this->hasOne('App\Models\Account');
    }
}