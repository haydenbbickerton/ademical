<?php

/*
 * Right now, this doesn't do anything other than 
 * broadcast the event to pusher.
 */

namespace App\Events\User;

use App\Models\User;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserLoggedIn extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user) 
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['user.'.$this->user->id];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        try
        {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::fromUser($this->user)) 
            {
                return ['success' => false];
            }
        } 
        catch(JWTException $e)
        {
            // something went wrong whilst attempting to encode the token
            return ['success' => false];;
        }

        return ['success' => true, 'token' => $token];
        //$token = JWTAuth::fromUser($this->user);
        //return ['token' => $token];
    }
}
