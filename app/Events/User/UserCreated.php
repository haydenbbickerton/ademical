<?php

namespace App\Events\User;

use App\Models\User;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserCreated extends Event implements ShouldBroadcast
{
    use SerializesModels;
    
    public $user;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user) 
    {
        $this->user = $user;
    }
    
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn() 
    {
        return ['user.' . $this->user->id];
    }
    
    /**
     * Get the broadcast event name.
     *
     * @return string
     */
    /*public function broadcastAs() 
    {
        return 'ademical.user-ready-for-login';
    }*/
}
