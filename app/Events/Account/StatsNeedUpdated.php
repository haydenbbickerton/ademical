<?php

namespace App\Events\Account;

use App\Models\Account;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StatsNeedUpdated extends Event
{
    use SerializesModels;

    public $account;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Account $account) 
    {
        $this->account = $account;
    }
}
