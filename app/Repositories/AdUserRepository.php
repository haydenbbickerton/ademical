<?php

namespace App\Repositories;

use App\Traits\CustomerService;
use App\Traits\ReportService;
use Config;
use App\Models\AdUser;
use App\Presenters\AdUserPresenter;
use App\Contracts\Repositories\AdUserRepository as AdUserRepositoryInterface;

use App\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class AdUserRepository
 * @package namespace App\Repositories;
 */
class AdUserRepository extends BaseRepository implements AdUserRepositoryInterface
{
    
    use CustomerService, ReportService;

    /**
     * The User who's AdWords account we grab
     *
     */
    protected $user;

    /**
     * @var @aduser
     */
    protected $aduser;

    protected $oauth2Info;


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AdUser::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /*public function presenter()
    {
        return AdUserPresenter::class;
    }*/

    /**
     * @throws RepositoryException
     */
    public function resetAdUser()
    {
        $this->aduser = $this->app->make(AdUser::class);
    }

    /**
     * Initalize the adwords user.
     * @return App\Models\AdUser
     */
    public function initAdUser($user)
    {
        $this->user = $user;

        $this->oauth2Info = [
            'client_id' => Config::get('services.google.client_id'),
            'client_secret' => Config::get('services.google.client_secret'),
            'access_token' => $this->user->access_token,
            'refresh_token' => $this->user->refresh_token,
            'expires_in' => $this->user->token_expires_in,
            'timestamp' => $this->user->token_timestamp 
        ];

        $this->model->SetOAuth2Info($this->oauth2Info);
        $this->model->SetDeveloperToken(Config::get('services.google.adwords.developer_token'));
        $this->model->SetUserAgent(Config::get('services.google.adwords.user_agent'));

        $result = $this->model;

        //$this->resetAdUser();

        return $result;
    }

    /**
     * Get the AdUser model
     * @return App\Models\AdUser
     */
    public function getAdUser()
    {
        $result = $this->model;
        //$this->resetAdUser();
        return $result;
    }

    /**
     * Refresh the access token of the current Aduser
     * @return array
     */
    public function refreshAccessToken()
    {
        return $this->model->GetDefaultOAuth2Handler()->RefreshAccessToken($this->oauth2Info);
    }
}




