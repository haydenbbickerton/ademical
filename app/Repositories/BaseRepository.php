<?php

namespace App\Repositories;


use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Repository\Eloquent\BaseRepository as DefaultBaseRepository;


/**
 * Class BaseRepository
 * @package namespace App\Repositories;
 */
abstract class BaseRepository extends DefaultBaseRepository
{

    /**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        return $this->model = $model;
    }

    /**
     * See if record with field and value exists
     *
     * @param $field
     * @param $value
     * @return mixed
     */
    public function exists($field, $value = null)
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->where($field,'=',$value)->exists();
        $this->resetModel();
        return $this->parserResult($model);
    }

    /**
     * Get the first record matching the attributes or create it.
     *
     * @param array $attributes
     * @return mixed
     */
    public function firstOrCreate(array $attributes)
    {
        $model = $this->model->firstOrCreate($attributes);
        $this->resetModel();
        return $this->parserResult($model);
    }

    /**
     * Find and get the record or create it.
     *
     * @param  array  $attributes
     * @return static
     */
    public function findOrCreate($id, array $attributes)
    {
        if ($this->exists('id', $id))
        {
            return $this->find($id);
        }

        return $this->create($attributes);
    }
}