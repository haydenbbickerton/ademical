<?php

namespace App\Repositories\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Carbon\Carbon;

/**
 * Class StatsByAccountCriteria
 *
 * This criteria will limit stats to the account requested
 *
 * ex - http://app.ademical.com/accounts/1234567/stats
 * 		will only get stats for account id 1234567
 */
class StatsByDatesCriteria implements CriteriaInterface
{
    
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $startDate  = $this->request->get('startDate', Carbon::now()->subMonths(1));
        $endDate    = $this->request->get('endDate', Carbon::now());

        // If the Dates are the same, that means they chose 1 day only.
        // So we'll use a where query instead of whereBetween
        if (Carbon::parse($startDate)->toDateString() == Carbon::parse($endDate)->toDateString()) {
            $model = $model->where('date', '=', Carbon::parse($startDate)->toDateString());
        } else {
            $model = $model->whereBetween('date', [Carbon::parse($startDate)->toDateString(), Carbon::parse($endDate)->toDateString()]);
        }



        return $model;
    }
}