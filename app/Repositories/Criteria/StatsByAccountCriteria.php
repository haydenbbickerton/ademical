<?php

namespace App\Repositories\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class StatsByAccountCriteria
 *
 * This criteria will limit stats to the account requested
 *
 * ex - http://app.ademical.com/accounts/1234567/stats
 * 		will only get stats for account id 1234567
 */
class StatsByAccountCriteria implements CriteriaInterface
{
    
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('account_id','=', $this->request->account_id )
                        ->orderBy('date', 'asc');

        return $model;
    }
}