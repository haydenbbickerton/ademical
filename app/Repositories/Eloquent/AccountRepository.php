<?php

namespace App\Repositories\Eloquent;

use DB;
use Carbon\Carbon;
use Log;
use Config;

use Illuminate\Http\Request;

use App\Models\Account;
use App\Models\User;
use App\Presenters\AccountPresenter;
use App\Contracts\Repositories\AccountRepository as AccountRepositoryInterface;

// Imports from Google Adwords php library
use App\Models\AdWordsUser as AdWordsUser;
use Selector;

use App\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;


/**
 * Class AccountRepository
 * @package namespace App\Repositories\Eloquent;
 */
class AccountRepository extends BaseRepository implements AccountRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Account::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Create an account for the given customer
     * 
     * @param array $customer
     * @return mixed
     */
    public function create(array $customer)
    {

        return $this->model->create($customer);
    }

    /**
     * Link a manager account to a client account
     * @param  App\Models\Account $manager
     * @param  App\Models\User $user
     * @return void
     */
    public function linkUser($manager, $user)
    {
        $manager->user()->associate($user); 
        $manager->save();
    }

    /**
     * Link a manager account to a client account
     * @param  App\Models\Account $manager
     * @param  App\Models\Account $client
     * @return void
     */
    public function linkClient($manager, $client)
    {
        $manager->accounts()->attach($client);
        $manager->save();
    }

    /**
     * Update the managed accounts.
     * @param array $clients
     * @param App\Models\Account $manager
     * @return void
     */
    public function updateManagedAccounts(array $clients, $manager)
    {
        if ( ! is_null($clients)) 
        {
            $clients = collect($clients);
            
            // Now we'll loop through each client account, and
            // create an account for it.
            $clients->each(function ($client, $key) use ($manager) {
                if (!$this->exists('id', $client['id'])) // Account doesn't exist, make one
                {
                    $clientaccount = $this->create((array) $client);
                    $this->linkClient($manager, $clientaccount);
                }
                
            });
        }
    }

}
