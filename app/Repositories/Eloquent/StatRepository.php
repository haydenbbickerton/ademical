<?php

namespace App\Repositories\Eloquent;

use DB;
use Carbon\Carbon;
use Log;
use Config;

use Illuminate\Http\Request;

use App\Models\Stat;
use App\Presenters\StatPresenter;
use App\Contracts\Repositories\StatRepository as StatRepositoryInterface;
use App\Repositories\Criteria\StatsByAccountCriteria;
use App\Repositories\Criteria\StatsByDatesCriteria;

use App\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;


/**
 * Class AccountRepository
 * @package namespace App\Repositories\Eloquent;
 */
class StatRepository extends BaseRepository implements StatRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Stat::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(StatsByAccountCriteria::class));
        $this->pushCriteria(app(StatsByDatesCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        return $this->model->create($data);
    }
}