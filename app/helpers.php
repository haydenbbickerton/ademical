<?php

/*
 * Random functions
 */

function getS3FileUrl($filename) {
	$bucket = Config::get('filesystems.disks.s3.bucket');
    $s3 = Storage::disk('s3');
    return Storage::disk('s3')->getDriver()->getAdapter()->getClient()->getObjectUrl($bucket, $filename);
}

function microAmountConvert($amount) {
    return $amount / 1000000;
}

/*
|--------------------------------------------------------------------------
|	Time Helpers
|--------------------------------------------------------------------------
|
| Some helpers to deal with time
|
*/

/**
 * Calculate the tick to get right now.
 * It operates on 48 ticks in a day (30 minute segments)
 *
 * @return int $tick
 */
function getTick()
{
    $carbon = new Carbon\Carbon();

    // Double the hours, because Carbon does 24 hour days
    $tick = $carbon->hour * 2;

    // Add tick if at 30 minute mark
    if (roundToHalfHour() == 30) {
        $tick++;
    }

    // Add 2 ticks if at 60 minute mark,
    // because that's the same as the next hour.
    if (roundToHalfHour() == 60) {
        $tick++;
        $tick++;
    }

    return $tick;
}

/**
 * Round the time to nearest half hour.
 * 
 * @return int $rounded
 */
function roundToHalfHour()
{
    $carbon = new Carbon\Carbon();
    $minutes = $carbon->minute;
    
    $rounded = 30 * round($minutes / 30);

    return $rounded;
}

/**
 * Convert the Human Readable Timezone to the actual Timezone
 * List taken from https://github.com/tamaspap/timezones
 * 
 * @param  string
 * @return string
 */
function convertHumanTimezone($timezone)
{
    $timezones = config('timezones.gmt');

    return $timezones[$timezone];
}