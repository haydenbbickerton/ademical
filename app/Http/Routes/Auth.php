<?php

/*
 * Auth Routes
 */

Route::get('auth', [
    'as' => 'auth', 'uses' => 'Auth\AuthController@show'
]);

Route::get('auth/redirect', [
    'as' => 'auth.redirect', 'uses' => 'Auth\AuthController@redirect'
]);

Route::get('auth/proxy', [
    'as' => 'auth.proxy', 'uses' => 'Auth\AuthController@proxy'
]);

Route::get('auth/logout', [
	'as' => 'auth.logout', 'uses' => 'Auth\AuthController@getLogout'
]);