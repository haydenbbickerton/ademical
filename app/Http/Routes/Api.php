<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\v1'], function ($api) {

    /*
     * Users
     */

    $api->group(['prefix' => 'users'], function ($api) {
        $api->get('/', ['as' => 'api.users.me', 'uses' => 'UserController@me']);
        $api->post('/', ['as' => 'api.users.store', 'uses' => 'UserController@store']);

        $api->get('/fire', ['as' => 'api.users.fire', 'uses' => 'UserController@fire']);
        $api->get('/refresh', ['as' => 'api.users.refresh', 'uses' => 'UserController@refresh']);

        $api->group(['prefix' => '{user_id}', 'middleware' => ['compareUser']], function ($api) {

            $api->post('/billing', ['as' => 'api.users.billing', 'uses' => 'UserController@storeBilling']);
            $api->delete('/billing', ['as' => 'api.users.billing', 'uses' => 'UserController@destroyBilling']);
        
        });


        $api->group(['prefix' => 'billing'], function ($api) {

            $api->post('/', 'SubscriptionController@subscribe');
            $api->put('/', 'SubscriptionController@changeSubscriptionPlan');
            $api->delete('/', 'SubscriptionController@cancelSubscription');
            $api->post('resume', 'SubscriptionController@resumeSubscription');
            $api->put('card', 'SubscriptionController@updateCard');
            $api->put('vat', 'SubscriptionController@updateExtraBillingInfo');
            $api->get('invoices/{invoice_id}', 'SubscriptionController@generateInvoiceLink');
            $api->get('invoices', 'SubscriptionController@getInvoices');

        });
 


        /*
         * For the time being, I'm disableing /api/user/{id} routes.
         * I don't think you would ever need to access another user directly.
         * If you are a manager, you only need account info, which can go through
         * the /api/account/{id} prefix. 
         */
        //$api->get('{id}', ['as' => 'api.user.show', 'uses' => 'UserController@show']);
        //$api->get('{id}/account', ['as' => 'api.user.account', 'uses' => 'UserController@account']);
    });

    $api->group(['prefix' => 'accounts'], function ($api) {

        $api->group(['prefix' => '{account_id}', 'middleware' => ['api.auth', 'compareAccount']], function ($api) {

            $api->get('/', ['as' => 'api.accounts.me', 'uses' => 'AccountController@show']);
            $api->put('activate', 'AccountController@activate');
            $api->delete('activate', 'AccountController@deActivate');

            $api->group(['prefix' => 'stats'], function ($api) {

                $api->get('/', ['as' => 'api.accounts.stats', 'uses' => 'StatController@index']);
        
            });
        
        });

    });


    $api->group(['prefix' => 'subscriptions'], function ($api) {

        $api->get('plans', ['as' => 'api.subscriptions.plans', 'uses' => 'SubscriptionController@getPlans']);

        $api->group(['prefix' => 'coupon'], function ($api) {

            $api->get('{code}', ['as' => 'api.subscriptions.coupon', 'uses' => 'SubscriptionController@getCoupon']);

        });

    });


});