<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use Illuminate\Http\Request;
use Spark;
use Storage;
use Config;

use App\Http\Requests;
use Illuminate\Routing\Controller;
use Dingo\Api\Routing\Helpers;

use App\Http\Requests\SubscriptionRequest;
use App\Repositories\Eloquent\UserRepository;

use Stripe\Stripe;
use Stripe\Coupon as StripeCoupon;
use Stripe\Customer as StripeCustomer;
use App\Subscriptions\Coupon;

// Events
use App\Events\User\Subscribed;
use App\Events\User\SubscriptionResumed;
use App\Events\User\SubscriptionCancelled;
use App\Events\User\SubscriptionPlanChanged;


use App\Transformers\UserTransformer;
use App\Models\User;

class SubscriptionController extends Controller
{
    use Helpers;

    public function __construct(UserRepository $user)
    {
        $this->middleware('api.auth', ['except' => [
                                                    'getPlans',
                                                    'getCoupon',
                                                    'downloadInvoice'
                                                    ]]);
        $this->user = $user;
    }

    /**
     * Get all of the plans defined for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPlans()
    {
        return response()->json(Spark::plans());
    }

    /**
     * Get the coupon for a given code.
     *
     * Used for the registration page.
     *
     * @param  string  $code
     * @return \Illuminate\Http\Response
     */
    public function getCoupon($code)
    {
        Stripe::setApiKey(config('services.stripe.secret'));

        if (count(Spark::plans()) === 0) {
            abort(404);
        }

        try {
            return response()->json(
                Coupon::fromStripeCoupon(StripeCoupon::retrieve($code))
            );
        } catch (Exception $e) {
            abort(404);
        }
    }

    /**
     * Subscribe the user to a new plan.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function subscribe(SubscriptionRequest $request)
    {

        // Make sure they don't already have a subscription
        if ($this->user()->subscribed()) {
            return response()->json(['error' => 'User already has a subscription. Please edit or resume the existing subscription.'], 400);
        }

        $stripeCustomer = $this->user()->stripe_id
                ? $this->user()->subscription()->getStripeCustomer() : null;

        $this->user->createSubscriptionOnStripe($request, $this->user(), $stripeCustomer);

        event(new Subscribed($this->user()));

        return $this->user->getCurrentUser();
    }

    /**
     * Change the user's subscription plan.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changeSubscriptionPlan(Request $request)
    {
        $rules = [
            'lastFour'  => ['required', 'numeric', 'min:0000', 'max:9999'],
            'plan'      => ['required', 'in:ademical_mcm_plus_monthly,ademical_mcm_premium_monthly']
        ];

        $input = app('request')->only('lastFour', 'plan');

        $validator = app('validator')->make($input, $rules);

        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\DeleteResourceFailedException('The last four digits must be integers between 0000-9999');
        }

        // Make sure they correctly enteried the last four digits of the credit card on file...
        if ($input['lastFour'] === $this->user()->last_four) {
            $plan = Spark::plans()->find($request->plan);

            // Do the plan swapping...
            $this->user()->subscription($input['plan'])
                         ->maintainTrial()->prorate()->swapAndInvoice();
            
            event(new SubscriptionPlanChanged($this->user()));

            // Don't send any data back, just tell the client to reload.
            return $this->response->noContent()->setStatusCode(205);
        } else {
            throw new \Dingo\Api\Exception\UpdateResourceFailedException('The given number does not match the card number on file');
        }
    }

    /**
     * Update the user's billing card information.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateCard(Request $request)
    {
        $this->validate($request, [
            'stripe_token' => 'required',
        ]);

        $this->user()->updateCard($request->stripe_token);

        return $this->users->getCurrentUser();
    }

    /**
     * Update the extra billing information for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateExtraBillingInfo(Request $request)
    {
        $this->user()->extra_billing_info = $request->text;

        $this->user()->save();
    }

    /**
     * Cancel the user's subscription.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelSubscription(Request $request)
    {
        $rules = [
            'lastFour' => ['required', 'numeric', 'min:0000', 'max:9999']
        ];

        $input = app('request')->only('lastFour');

        $validator = app('validator')->make($input, $rules);

        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\DeleteResourceFailedException('The last four digits must be integers between 0000-9999');
        }

        // Make sure they correctly enteried the last four digits of the credit card on file...
        if ($input['lastFour'] === $this->user()->last_four) {

            $this->user()->subscription()->cancelAtEndOfPeriod();
            event(new SubscriptionCancelled($this->user()));

            // Don't send any data back, just tell the client to reload.
            return $this->response->noContent()->setStatusCode(205);

        } else {
            throw new \Dingo\Api\Exception\DeleteResourceFailedException('The given number does not match the card number on file');
        }

    }

    /**
     * Resume the user's subscription.
     *
     * @return \Illuminate\Http\Response
     */
    public function resumeSubscription()
    {
        $this->user()->subscription($this->user()->stripe_plan)->skipTrial()->resume();
        event(new SubscriptionResumed($this->user()));

        // Don't send any data back, just tell the client to reload.
        return $this->response->noContent()->setStatusCode(205);
    }

    /**
     * Put invoice in s3 bucket and return link
     *
     * @param  string  $invoice_id
     * @return \Illuminate\Http\Response
     */
    public function generateInvoiceLink(Request $request, $invoice_id)
    {
        $data = Spark::generateInvoicesWith();

        $invoice = $this->user()->findInvoiceOrFail($invoice_id);
        $filename = 'v1/invoices/ademical-'.$invoice->date()->month.'-'.$invoice->date()->year.'-'.$invoice->id.'.pdf';

        // If the invoice hasn't already been stored, create
        // it and store it in the bucket.
        if (!Storage::disk('s3')->has($filename)) {
            $invoice_file = $invoice->pdf($data);
            Storage::disk('s3')->put($filename, $invoice_file);
        }

        $filelink = getS3FileUrl($filename);

        //return $this->response->created($filelink);
        return $this->response->array(collect(['url' => $filelink]));

    }

    /**
     * Returns a collection with all the users invoices.
     */
    public function getInvoices()
    {
        $invoices = collect($this->user()->invoices());
        return $this->collection($invoices, new \App\Transformers\InvoiceTransformer);
    }
}
