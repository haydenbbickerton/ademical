<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Dingo\Api\Routing\Helpers;

use App\Models\Account;
use App\Transformers\UserTransformer;
use App\Transformers\AccountTransformer;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Contracts\Repositories\AccountRepository;
use App\Contracts\Repositories\StatRepository;
use App\Presenters\StatPresenter;


/**
 * User resource representation.
 *
 * @Resource("Users", uri="/api/users")
 */
class StatController extends Controller
{
    use Helpers;

    public function __construct(AccountRepository $accountRepository,
                                StatRepository $statRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->statRepository = $statRepository;

        $this->statRepository->setPresenter(StatPresenter::class);
    }

    public function index()
    {
        $stats = $this->statRepository->paginate($limit = 31);
        return $stats;
    }
}
