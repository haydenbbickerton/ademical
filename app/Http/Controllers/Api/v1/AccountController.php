<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Dingo\Api\Routing\Helpers;

use App\Models\Account;
use App\Transformers\UserTransformer;
use App\Transformers\AccountTransformer;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Contracts\Repositories\AccountRepository;
use App\Presenters\AccountPresenter;

/**
 * Account resource representation.
 *
 * @Resource("Accounts", uri="/api/accounts")
 */
class AccountController extends Controller
{
    use Helpers;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->accountRepository->setPresenter(AccountPresenter::class);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($account_id)
    {
        ///$account = Account::findOrFail($account_id);
        //return $this->response->item($account, new AccountTransformer);
        
        $account = $this->accountRepository->find($account_id);
        return $account;
    }

    /**
     * Activate the specified account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $account_id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $account_id)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();

        $account = Account::find($account_id);
        //$account = $this->accountRepository->find($account_id);

        if ($user->account->canActivateAccount($account)) {

            try {
                $this->accountRepository
                    ->update([
                        'active' => true,
                        'activated_at' => Carbon::now(),
                        'activated_by' => $user->account->id,
                        ], $account_id);
                return response()->json(['message' => 'The account has been activated successfully. Refresh the page to see the changes.'], 200);
            } catch (Exception $e) {
                throw new \Dingo\Api\Exception\UpdateResourceFailedException('The account could not be activated.');
            }
            
        } else {
            throw new \Dingo\Api\Exception\UpdateResourceFailedException('The account could not be activated.');
        }
    }

    /**
     * De-Activate the specified account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $account_id
     * @return \Illuminate\Http\Response
     */
    public function deActivate(Request $request, $account_id)
    {
        $user = app('Dingo\Api\Auth\Auth')->user();

        $account = Account::find($account_id);

        if ($user->account->canDeActivateAccount($account)) {

            try {
                $this->accountRepository
                    ->update([
                        'active' => false
                        ], $account_id);
                return response()->json(['message' => 'The account has been de-activated successfully. Refresh the page to see the changes.'], 200);
            } catch (Exception $e) {
                throw new \Dingo\Api\Exception\DeleteResourceFailedException('The account could not be de-activated.');
            }
            
        } else {
            throw new \Dingo\Api\Exception\DeleteResourceFailedException('The account could not be de-activated.');
        }
    }
}
