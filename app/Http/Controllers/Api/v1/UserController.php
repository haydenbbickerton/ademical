<?php
namespace App\Http\Controllers\Api\v1;

use Auth;
use Log;
use Event;
use Config;

use AdWordsUser;
use Selector;

//use App\Events\UserCreated;
use App\Events\TokensAreExpired;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Dingo\Api\Routing\Helpers;
use Carbon\Carbon; 

use App\Models\User;
use App\Http\Requests\UserPostRequest;
use App\Http\Requests\UserBillingPostRequest;
use App\Transformers\UserTransformer;
use App\Transformers\AccountTransformer;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Contracts\Repositories\UserRepository;
use App\Contracts\Repositories\AccountRepository;
use App\Contracts\Repositories\AdUserRepository;
use App\Contracts\Repositories\StatRepository;

/**
 * User resource representation.
 *
 * @Resource("Users", uri="/api/users")
 */
class UserController extends Controller
{
    use DispatchesJobs, Helpers;
    
    public function __construct(UserRepository $userRepository, 
                                AccountRepository $accountRepository,
                                AdUserRepository $adUserRepository,
                                StatRepository $statRepository) 
    {
        $this->userRepository = $userRepository;
        $this->accountRepository = $accountRepository;
        $this->adUserRepository = $adUserRepository;
        $this->statRepository = $statRepository;
        $this->middleware('api.auth', ['except' => ['store']]);
    }
    
    /**
     * Get users, paginate by 25.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        
        //$users = User::paginate(25);
        
        //return $this->response->paginator($users, new UserTransformer);
        
    }
    
    /**
     * Get all users.
     *
     * @return \Illuminate\Http\Response
     */
    public function all() 
    {
        
        //$users = User::all();
        
        //return $this->response->collection($users, new UserTransformer);
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPostRequest $request) 
    {
        
        $profile = collect($request->all());
        
        if ($this->userRepository->exists('email', $profile['email'])) 
        {
            
            // user found
            $user = $this->userRepository->findByField('email', $profile['email'])->first();
            //$user = User::where('email', '=', $profile['email'])->first();
        } 
        else if (!isset($profile['refresh_token']) or is_null($profile['refresh_token'])) 
        {
            
            /*
             * The order of this is important. A user cannot be created without a refresh_token,
             * so I don't have to check for a user on this second one (if they did exist, they
             * would have gotten caught in the first if).
             *
             * Anyways, the user doesn't exist and we didn't get a refresh_token. Force them to
             * re-approve through Google OAuth2.
            */
            return response()->json(['error' => 'Invalid refresh token. Force Re-Approval.', 'action' => 'force_auth_approval'], 401);
        } 
        else
        {
            try {
                $userInfo = [
                    'name' => $profile['name'], 
                    'first_name' => $profile['first_name'], 
                    'last_name' => isset($profile['last_name']) ? $profile['last_name'] : null, 
                    'email' => $profile['email'], 
                    'google_id' => $profile['id'], 
                    'access_token' => $profile['access_token'], 
                    'refresh_token' => $profile['refresh_token'], 
                    'picture' => $profile['picture'], 
                    'gender' => isset($profile['gender']) ? $profile['gender'] : null,
                    'language' => isset($profile['language']) ? $profile['language'] : null,
                    'display_name' => isset($profile['displayName']) ? $profile['displayName'] : null
                ];

                $user = $this->userRepository->create($userInfo);
            } catch(\Illuminate\Database\QueryException $e) {
                return response()->json(['error' => 'Something went wrong. Sorry about that, try again later.'], 500);
            }
            
        }
            
            Auth::login($user);
            return $this->response->item($user, new UserTransformer)->addMeta('', '');
        }
        
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function fire() 
        {
            $tick = getTick();

            $this->dispatch(new \App\Jobs\UpdateAccountStats($tick));

/*
            $user = $this->userRepository->getCurrentUser();

            $this->adUserRepository->initAdUser($user);

            //sd($this->adUserRepository->customerService());

            $aduser = $this->adUserRepository->getAdUser();

            $report = $this->adUserRepository->getReport($aduser, 7857125579);

            //sd($report);

            $report['account_id'] = $user->account->id;

            $stat = $this->statRepository->create($report);*/

        }
        
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function refresh() 
        {
            $user = app('Dingo\Api\Auth\Auth')->user();
            sd($this->dispatch(new \App\Jobs\RefreshAccessTokens()));
            
            sd(Event::fire(new TokensAreExpired()));
        }
        
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id) 
        {
            
            //$user = User::findOrFail($id);
            
            //return $this->response->item($user, new UserTransformer);
            
            
        }
        
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function me() 
        {
            $user = app('Dingo\Api\Auth\Auth')->user();
            
            return $this->response->item($user, new UserTransformer);
        }
        
        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id) 
        {
            
            //
            
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id) 
        {
            
            //
            
        }
        
        /**
         * Get the adwords account for this user.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function account($id = null) 
        {
            
            //$account = User::findOrFail($id)->account;
            
            //return $this->response->item($account, new AccountTransformer);
            
            
        }
        
        /**
         * Create the user subscription.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function storeBilling(UserBillingPostRequest $request, $user_id) 
        {
            
            $plans = ['1' => 'ademical_mcm_basic_monthly', '2' => 'ademical_mcm_plus_monthly', '3' => 'ademical_mcm_premium_monthly'];
            
            $input = $request->all();
            
            $user = User::find($user_id);
            
            $user->subscription($input['plan'])->create($input['id'], ['email' => $user->email]);
            
            //$user->subscription('ademical_basic_account')->create($input['id']);
            
            return $this->response->item($user, new UserTransformer);
        }
        
        /**
         * Cancel the user subscription.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroyBilling($user_id) 
        {
            $user = User::find($user_id);
            $user->subscription()->cancel();
        }
    }
    