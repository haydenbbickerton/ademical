<?php

namespace App\Http\Middleware;

use Closure;

class CompareJWTToUserId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // This seems kind of messy, but I couldn't find a way to get route paremeters
        // from middleware :/
        $requested_user_id = app()->router->getCurrentRoute()->getParameter('user_id');

        $user = app('Dingo\Api\Auth\Auth')->user();

        /*
         * If the requested user (the one in the url) matches
         * the user to which the jwt token is assigned,
         */
        if ( (int) $requested_user_id == (int) $user->id) 
        {
            return $next($request);
        } else {
            throw new \Tymon\JWTAuth\Exceptions\TokenInvalidException('You are not authorized to view that user.', 401);
        }
        
    }
}
