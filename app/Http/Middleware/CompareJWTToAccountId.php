<?php

namespace App\Http\Middleware;

use Closure;

class CompareJWTToAccountId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // This seems kind of messy, but I couldn't find a way to get route paremeters
        // from middleware :/
        $account_id = app()->router->getCurrentRoute()->getParameter('account_id');

        $account = \App\Models\Account::find($account_id);

        $user = app('Dingo\Api\Auth\Auth')->user();

        //$account_id = $user->account;

        /*
         * If the requested account (the one in the url) matches
         * the account to which the jwt token is assigned,
         * OR
         * If the requested account is managed by the account to which
         * the jwt token is assigned.
         *
         * This let's us prevent people from accessing other accounts,
         * while still allowing managers to access accounts that they manage.
         */
        if ($account->id == (int) $user->account->id ||
            $user->account->managesAccount($account)
            ) 
        {
            return $next($request);
        } else {
            throw new \Tymon\JWTAuth\Exceptions\TokenInvalidException('You are not authorized to view that account.', 401);
        }
        
    }
}
