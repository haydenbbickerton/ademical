<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserBillingPostRequest extends Request
{
    private $billingRules = [
        'id' => 'required|max:100',
        'object' => 'required|in:token'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd('huh');
        return $this->billingRules;
    }
}
