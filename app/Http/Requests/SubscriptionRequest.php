<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubscriptionRequest extends Request
{
    
    /*card:
    {
        address_city: null
        address_country: null
        address_line1: null
        address_line1_check: null
        address_line2: null
        address_state: null
        address_zip: null
        address_zip_check: null
        brand: "Visa"
        country: "US"
        cvc_check: "unchecked"
        dynamic_last4: null
        exp_month: 2
        exp_year: 2017
        funding: "credit"
        id: "card_7Qko19N7QpgYAL"
        last4: "4242"
        metadata:
        {}
        name: "Hayden Test Three"
        object: "card"
        tokenization_method: null
    }
    client_ip: "173.23.174.215"
    created: 1448657925
    id: "tok_7Qko4uVAMv8aS4"
    livemode: false
    object: "token"
    plan: 2
    type: "card"
    used: false
    */
   
    private $rules = [
            'id' => 'required|max:100', 
            'object' => 'required|in:token', 
            'plan' => 'required', 
            ];
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() 
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() 
    {
        return $this->rules;
    }
}
