<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserPostRequest extends Request
{
    private $profileRules = [
        'kind' => 'required',
        'objectType' => 'required|in:person',
        'id' => 'required|numeric',
        'access_token' => 'required',
        'name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'first_name' => 'required|alpha_dash',
        'last_name' => '',
        'email' => 'required|email',
        'picture' => 'required|url'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->profileRules;
    }
}
