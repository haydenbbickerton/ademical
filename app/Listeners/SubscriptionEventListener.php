<?php

namespace App\Listeners;

use App\Events\UserLoggedIn;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use App\Models\User;
use App\Models\Account;
use Event;

use App\Events\User\Subscribed;
use App\Events\User\SubscriptionCancelled;
use App\Events\User\SubscriptionResumed;
use App\Events\User\SubscriptionPlanChanged;

class SubscriptionEventListener implements ShouldQueue
{
    use SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
    }

    /**
     * Handle user subscribed events.
     */
    public function onSubscribed(Subscribed $event)
    {
        $user = $event->user;
        $this->beautymail->send('emails.subscriptions.subscribed', [], function($message)
        {
            $message
                ->from('hayden@frankandmaven.com')
                ->to($user->email, $user->name)
                ->subject('Subscribed!');
        });
    }

    /**
     * Handle user cancellation events.
     */
    public function onSubscriptionCancelled(SubscriptionCancelled $event)
    {
        $user = $event->user;
        $this->beautymail->send('emails.subscriptions.canceled', [], function($message) use ($user)
        {
            $message
                ->from('hayden@frankandmaven.com')
                ->to($user->email, $user->name)
                ->subject('Canceled!');
        });
    }

    /**
     * Handle user cancellation events.
     */
    public function onSubscriptionResumed(SubscriptionResumed $event)
    {
        $user = $event->user;
        $this->beautymail->send('emails.subscriptions.resumed', [], function($message) use ($user)
        {
            $message
                ->from('hayden@frankandmaven.com')
                ->to($user->email, $user->name)
                ->subject('Resumed!');
        });
    }

    /**
     * Handle user cancellation events.
     */
    public function onSubscriptionPlanChanged(SubscriptionPlanChanged $event)
    {
        $user = $event->user;
        $this->beautymail->send('emails.subscriptions.plan_changed', [], function($message) use ($user)
        {
            $message
                ->from('hayden@frankandmaven.com')
                ->to($user->email, $user->name)
                ->subject('Plan Changed!');
        });
    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\User\Subscribed',
            'App\Listeners\SubscriptionEventListener@onSubscribed'
        );

        $events->listen(
            'App\Events\User\SubscriptionCancelled',
            'App\Listeners\SubscriptionEventListener@onSubscriptionCancelled'
        );

        $events->listen(
            'App\Events\User\SubscriptionResumed',
            'App\Listeners\SubscriptionEventListener@onSubscriptionResumed'
        );

        $events->listen(
            'App\Events\User\SubscriptionPlanChanged',
            'App\Listeners\SubscriptionEventListener@onSubscriptionPlanChanged'
        );

    }
}
