<?php

namespace App\Listeners;

use App\Events\StatsNeedUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use AdWordsUser;
use Log;
use Config;

use App\Models\Account;
use App\Models\Stat;

class GetAdwordsAccountStats
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatsNeedUpdated  $event
     * @return void
     */
    public function handle(StatsNeedUpdated $event)
    {
        Log::info('Starting the GetAdwordsAccountStats Listener');

        $account = $event->account;
    }
}
