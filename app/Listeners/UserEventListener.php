<?php

namespace App\Listeners;

use App\Events\UserLoggedIn;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use App\Models\User;
use App\Models\Account;
use Event;

class UserEventListener implements ShouldQueue
{
    use SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\User\UserCreated',
            'App\Listeners\UserEventListener@onUserCreated'
        );

        $events->listen(
            'auth.login',
            'App\Listeners\UserEventListener@onUserLogin'
        );

    }
    
    /**
     * Handle user creation events.
     */
    public function onUserCreated($event) {
        //Event::fire(new \App\Events\UserCreated($user));
    }

    /**
     * Handle user login events.
     */
    public function onUserLogin($event) {
        Log::info($event);
        $user = $event;
        Event::fire(new \App\Events\User\UserLoggedIn($user));
    }



}
