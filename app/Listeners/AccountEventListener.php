<?php

namespace App\Listeners;

//use App\Events\UserWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use Config;
use Event;

use App\Contracts\Repositories\AccountRepository;
use App\Contracts\Repositories\AdUserRepository;
use App\Contracts\Repositories\UserRepository;

// Imports from Google Adwords php library
//use AdWordsUser;
//use Selector;

class AccountEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(AccountRepository $accountRepository, 
                                AdUserRepository $adUserRepository,
                                UserRepository $userRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->adUserRepository = $adUserRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        /*$events->listen(
            'App\Events\User\UserCreated',
            'App\Listeners\AccountEventListener@onUserCreated'
        );*/
        $events->listen(
            'auth.login',
            'App\Listeners\AccountEventListener@onUserLogin'
        );
    }

    public function onUserLogin($event)
    {
        Log::info('Starting the GetAdwordsAccount Listener');

        $user = $event;

        $this->adUserRepository->initAdUser($user);
        
        // If the user doesn't have an account,
        // let's make one for them.
        if (! ((bool) $user->account()->count()))
        {
            $customer = $this->adUserRepository->getCustomer();

            /*
             * It is possible for an account with the users id to exist, but 
             * the user does not exist. This will happen if a manager creates an account,
             * which creates accounts for their clients (who have not logged in yet).
             */
            $account = $this->accountRepository->findOrCreate($customer['id'], $customer);

            // Link the account to the user
            $this->accountRepository->linkUser($account, $user);

            //$managed = $this->adUserRepository->getManagedCustomers();
            //$this->accountRepository->updateManagedAccounts($managed, $account);

            //$this->accountRepository->updateManagedAccounts($this->adUserRepository->getAdUser(), $manager);
        } else {
            $account = $user->account;
        }

        $managed = $this->adUserRepository->getManagedCustomers();
        $this->accountRepository->updateManagedAccounts($managed->toArray(), $account);
    }

}
