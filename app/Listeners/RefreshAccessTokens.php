<?php

namespace App\Listeners;

use App\Events\TokensAreExpired;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use AdWordsUser;
use Log;
use Config;

use App\Models\User;

class RefreshAccessTokens implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TokensAreExpired  $event
     * @return void
     */
    public function handle(TokensAreExpired $event)
    {
        Log::info('Refreshing access tokens');
        $users = User::all();

        $users->each(function ($user, $key) {

            $this->RefreshToken($user);
            
        });
    }

    /**
     * Check account to see if it needs the token updated.
     * If so, update the token and save the account.
     *
     * @param Account $account
     */
    public function RefreshToken(User $user) 
    {

        $oauth2Info = array(
            'client_id' => Config::get('services.google.client_id'),
            'client_secret' => Config::get('services.google.client_secret'),
            'access_token' => $user->access_token,
            'refresh_token' => $user->refresh_token
        );

        /*
         * I don't really know why I can create the AdwordsUser
         * without passing in the Oauth2 credentials.
         */
               
        // See AdWordsUser constructor
        // @param string $authenticationIniPath
        // @param string $developerToken (required)
        // @param string $userAgent (required)
        // @param string $clientCustomerId
        // @param string $settingsIniPath
        // @param array $oauth2Info
        $aduser = new AdWordsUser(
                                null, 
                                Config::get('services.google.adwords.developer_token'), 
                                Config::get('services.google.adwords.user_agent'), 
                                null, 
                                null,
                                null
                                );

        $OAuth2Handler = $aduser->GetDefaultOAuth2Handler();

        // Pass in our old info and recieve the new info. Then save.

        $newInfo = $OAuth2Handler->RefreshAccessToken($oauth2Info);

        $user->access_token = $newInfo['access_token'];

        $user->save();

    }
}
