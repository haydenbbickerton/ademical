<?php

namespace App\Listeners;

//use App\Events\UserWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use Config;
use Event;

use App\Models\Account;

// Imports from Google Adwords php library
use AdWordsUser;
use Selector;

class StatsEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Account\StatsNeedUpdated',
            'App\Listeners\StatEventListener@updateStats'
        );
    }

    /**
     * Update the stats for this account.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function updateStats($event)
    {
        
      

        
    }

}
