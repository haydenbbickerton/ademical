<?php
namespace App\Jobs;

/*
 * TODO: Batch these requests
 *
 * https://developers.google.com/adwords/api/docs/guides/bestpractices
 */

use Log;
use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

use App\Contracts\Repositories\AccountRepository;
use App\Contracts\Repositories\AdUserRepository;
use App\Contracts\Repositories\StatRepository;

class UpdateAccountStats extends Job implements SelfHandling
{
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tick) 
    {
        Log::info('Running UpdateAccountStats Job');        
        $this->tick = $tick;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AccountRepository $accountRepository,
                           AdUserRepository $adUserRepository, 
                           StatRepository $statRepository) 
    {
        $this->accountRepository = $accountRepository;
        $this->adUserRepository = $adUserRepository;
        $this->statRepository = $statRepository;

        /*
         * TODO: Right now this only updates stats for people that
         * signed up through a manager. DIYers won't get updates!
         */
        // Get managers
        $managers = $this->accountRepository->findWhere([
            'can_manage' => true
        ]);

        $managers->each(function ($manager, $key) {

            // Make sure this manager has an active subscription
            if ($manager->user->subscribed()) {

                /*
                 * Of this managers accounts, get the accounts that
                 *     1 - Are active
                 *     2 - Are in this tick
                 */
                $accounts = $manager->accounts()->where([
                    //'update_tick' => $this->tick,
                    'active' => true
                ])->get();

                // We'll use the managers access to keys to do the work
                $accounts->each(function ($account, $key) use ($manager) {



                    // Initalize our AdUser
                    $this->adUserRepository->initAdUser($manager->user);
                    $aduser = $this->adUserRepository->getAdUser();


                    // This chunk of code will get stats for every day going back x days (defined in the loop start)
                    for ($i = 0; $i < 14; $i++) {

                        $maxDays = new Carbon();

                        $minDays = new Carbon();

                        $dateRange = [
                            'max' => $maxDays->subDays($i)->format('Ymd'),
                            'min' => $minDays->subDays($i + 1)->format('Ymd')
                        ];

                        $report = $this->adUserRepository->getReport($aduser, $account->id, null, null, 'CUSTOM_DATE', $dateRange);
                        $report['account_id'] = $account->id;

                        $stat = $this->statRepository->create($report);

                    }

                    // Get our report, assign it to this account.
                    //$report = $this->adUserRepository->getReport($aduser, $account->id);
                    //$report['account_id'] = $account->id;
                    

                    // Save this stat!
                    //$stat = $this->statRepository->create($report);
                });

            }
        });
    }
}
