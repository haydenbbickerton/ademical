<?php

namespace App\Jobs;

use Log;
use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Contracts\Repositories\AdUserRepository;
use App\Contracts\Repositories\UserRepository;

class RefreshAccessTokens extends Job implements SelfHandling, ShouldQueue
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() 
    {
        Log::info('Running RefreshAccessTokens Job');
    }

    /**
     * Refresh the Access Token for every user.
     *
     * @return void
     */
    public function handle(AdUserRepository $adUserRepository,
                           UserRepository $userRepository)
    {
        $this->adUserRepository = $adUserRepository;
        $this->userRepository = $userRepository;

        // Get just the columns needed for token refreshing
        $users = $this->userRepository->all($columns = [
                                                        'id',
                                                        'access_token',
                                                        'refresh_token',
                                                        'token_expires_in',
                                                        'token_timestamp'
                                                        ]);

        $users->each(function ($user, $key) {

            // Initalize AdUser and send the token refresh request
            $this->adUserRepository->initAdUser($user);
            $newTokenResult = $this->adUserRepository->refreshAccessToken();

            // Update the user values
            // I would use an update array, but I don't have the same names as
            // some of the return values :/
            $user->access_token = $newTokenResult['access_token'];
            $user->refresh_token = $newTokenResult['refresh_token'];
            $user->token_expires_in = $newTokenResult['expires_in'];
            $user->token_timestamp = $newTokenResult['timestamp'];

            $user->save();

        });
    }
}
