<?php

namespace App\Transformers;

use App\Models\AdUser;
use League\Fractal;

class AdUserTransformer extends Fractal\TransformerAbstract
{

	public function transform(AdUser $adUser)
	{
	    return [
            'id' => $adUser['customerId'],
            'date_time_zone' => $adUser['dateTimeZone'],
            'descriptive_name' => $adUser['descriptiveName'],
            'company_name' => $adUser['companyName'],
            'can_manage' => $adUser['canManageClients'],
            'test_account' => $adUser['testAccount'],
	    ];
	}

}