<?php
namespace App\Transformers;

use App\Models\Stat;
use League\Fractal;
use Carbon\Carbon;

class StatTransformer extends Fractal\TransformerAbstract
{
    public function transform(Stat $stat) 
    {
        return [
            'account_id' => $stat->account_id,
            'avg_cost' => $stat->avg_cost,
            'avg_cpc' => $stat->avg_cpc,
            'avg_position' => $stat->avg_position,
            'clicks' => $stat->clicks,
            'conversion_rate' => $stat->conversion_rate,
            'conversions' => $stat->conversions,
            'converted_clicks' => $stat->converted_clicks,
            'cost' => $stat->cost,
            'ctr' => $stat->ctr,
            'date' => $stat->date,
            'impressions' => $stat->impressions,
            'search_impression_share' => $stat->search_impression_share,
        ];
    }
}
