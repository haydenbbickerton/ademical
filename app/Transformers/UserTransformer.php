<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal;
use Carbon\Carbon;

use App\Transformers\BillingTransformer;
use App\Transformers\AccountTransformer;

class UserTransformer extends Fractal\TransformerAbstract
{

	/**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'account',
        'billing'
    ];

	public function transform(User $user)
	{
	    return [
	        'id'      => $user->id,
	        'first_name' => $user->first_name,
	        'last_name' => $user->last_name,
	        'email'   => $user->email,
            'picture' => $user->picture
	    ];
	}

    /**
     * Include Account
     *
     * @return League\Fractal\ItemResource
     */
    public function includeAccount(User $user)
    {
        
        return $this->item($user->account, new AccountTransformer);
    }

	/**
     * Include Billing
     *
     * @return League\Fractal\ItemResource
     */
    public function includeBilling(User $user)
    {
    	return $this->item($user, new BillingTransformer);
    }
}
