<?php

namespace App\Transformers;

use League\Fractal;
use Carbon\Carbon;

class InvoiceTransformer extends Fractal\TransformerAbstract
{

	public function transform(\Laravel\Cashier\Invoice $invoice)
	{
	    return [
	        'id'            => $invoice->id,
            'total'         => $invoice->total(),
            'date'          => $invoice->date(),
            'human_date'    => $invoice->dateString(),
            'link'          => url('users/billing/invoices', [$invoice->id])
	    ];
	}

}
