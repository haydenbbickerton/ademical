<?php

namespace App\Transformers;

use App\Models\User;
use App\Models\Account;
use League\Fractal;
use Carbon\Carbon;

use App\Transformers\UserTransformer;

class AccountTransformer extends Fractal\TransformerAbstract
{
	/**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'accounts',
        'user'
    ];

	public function transform(Account $account)
	{
	    return [
	        'id'      => $account->id,
            'descriptive_name'      => $account->descriptive_name,
            'company_name'      => $account->company_name,
            'description'      => $account->description,
            'can_manage'      => $account->can_manage,
            'is_managed'      => $account->is_managed,
            'test_account'      => $account->test_account,
            'active'      => $account->active,
            'date_time_zone' => $account->date_time_zone
	    ];
	}

	/**
     * Include Client Accounts
     *
     */
    public function includeAccounts(Account $account)
    {
        //sd((bool) count($account->accounts));
        if (! $account->hasManagedAccounts()) {
            return $this->null();
        }

    	return $this->collection($account->accounts()->get(), new AccountTransformer);
    }

    /**
     * Include User
     *
     */
    public function includeUser(Account $account)
    {
        if (is_null($account->user)) {
            return $this->null();
        }

        return $this->item($account->user, new UserTransformer);
    }

}