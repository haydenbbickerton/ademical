<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal;

class BillingTransformer extends Fractal\TransformerAbstract
{
	public function transform(User $user)
	{
	    return [
	    	'subscribed' => $user->subscribed(),
	    	'subscription_id' => $user->stripe_subscription,
	    	'plan' => $user->stripe_plan,
	        'on_trial' => $user->onTrial(),
	        'on_grace' => $user->onGracePeriod(),
	        'cancelled' => $user->cancelled(),
	        'subscription_ends_at' =>  $this->toDateFormat($user->subscription_ends_at),
	    ];
	}

	public function toDateFormat($val)
	{
		return is_null($val) ? null : $val->toDateTimeString();
	}
}