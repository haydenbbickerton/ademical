<?php

namespace App\Traits;

use App\Spark\Spark;

trait ManageAccounts
{
    /**
     * Determine if the account has any managed accounts.
     *
     * @return bool
     */
    public function hasManagedAccounts()
    {
        return (bool) count($this->accounts);
    }

    /**
     * Determine if the given account is managed by the account.
     *
     * @param  \App\Models\Account $account
     * @return bool
     */
    public function managesAccount($account)
    {
        if (is_null($account->id) || is_null($this->id)) 
        {
            return false;
        }

        // I'm using count because there could me multiple managers for a single account.
        // return $this->id === $account->pivot->manager_id;
        return (bool) count($this->accounts()->where('id','=',$account->id)->get());
    }

    /**
     * Get the active account for this manager account.
     *
     */
    public function activeAccounts()
    {
        if (is_null($this->id)) 
        {
            return false;
        }

        return $this->accounts()->where('active', '=', true);
    }

    /**
     * Determine if this account has active managed accounts
     *
     * @return bool
     */
    public function hasActiveAccounts()
    {
        if (is_null($this->id)) 
        {
            return false;
        }

        return (bool) $this->activeAccounts()->count();
    }

    /**
     * Determine if this account is able to activate the given account.
     *
     * TODO: Need to find a way to return the reasons. Right now it's not clear WHY 
     * an account can't be activated.
     *
     * @param  \App\Models\Account $account
     * @return bool
     */
    public function canActivateAccount(\App\Models\Account $account)
    {
        // Make sure we can resolve both manager and client accounts
        if (is_null($this->id) || is_null($account->id)) 
        {
            return false;
        }

        // Make sure the logged in account manages the account being activated
        if (!$this->managesAccount($account))
        {
            return false;
        }

        // Make sure the account isn't already active
        if ($account->active)
        {
            return false;
        }

        // Double check the user is subscripted (this feels redundant, if they weren't
        // subscribed they wouldn't have gotten this far).
        if (! $this->user->subscribed()) 
        {
            return false;
        }

        $plan = Spark::plans()->find($this->user->getStripePlan());

        if ((int) $this->activeAccounts()->count() >= (int) $plan->attributes['accounts_included']) 
        {
            // They are at or above their limit for active account on their current plan,
            // they cannot activate any more accounts.
            return false;
        }

        if ((bool) $account->managers->count() && $account->active) 
        {
            /*
             * For the time being, I'm disabling the ability of an account to have
             * multiple managers. Note that this should also trigger if they are
             * trying to activate an account that they already manage and have activated.
             */
            return false;
        }

        return true;
    }

    /**
     * Determine if this account is able to de-ctivate the given account.
     *
     * TODO: Need to find a way to return the reasons. Right now it's not clear WHY 
     * an account can't be activated.
     *
     * @param  \App\Models\Account $account
     * @return bool
     */
    public function canDeActivateAccount(\App\Models\Account $account)
    {
        // Make sure we can resolve both manager and client accounts
        if (is_null($this->id) || is_null($account->id)) 
        {
            return false;
        }

        // Make sure the logged in account manages the account being activated
        if (!$this->managesAccount($account))
        {
            return false;
        }

        // Make sure the account is active
        if (!$account->active)
        {
            return false;
        }

        // You aren't allowed to deactivate an account if it hasn't been active for
        // at least 31 days. This is to prevent people from activating/deativating
        // many accounts every other day to circumvent their subscription account limits.
        if ((int) \Carbon\Carbon::now()->diffInDays($account->activated_at) <= 31)
        {
            return false;
        }

        // You can't deactivate an account that you didn't activate
        if ($this->id !== $account->activated_by) {
            return false;
        }

        return true;
    }



    

}