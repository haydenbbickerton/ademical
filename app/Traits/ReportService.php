<?php

namespace App\Traits;

use Selector;
use ReportUtils;
use ReportDefinition;
use Predicate;

use League\Csv\Reader;

use App\Models\AdUser;

trait ReportService
{

    /**
     * @var $adwords_version
     */
    protected $adwords_version = 'v201509';

    /**
     * Default report type
     * @var string
     */
    protected $type = 'ACCOUNT_PERFORMANCE_REPORT';

    /**
     * Default date range type
     * @var string
     */
    protected $dateRangeType = 'YESTERDAY';

    /**
     * The fields we select from reports.
     *
     * https://developers.google.com/adwords/api/docs/appendix/reports/campaign-performance-report
     *
     * @var array
     */
    protected $reportFields = [
        //'account_name' => 'AccountDescriptiveName',
        //'all_conversions' => 'AllConversions',
        'avg_cpc' => 'AverageCpc',
        'avg_position' => 'AveragePosition',
        'clicks' => 'Clicks',
        'conversion_rate' => 'ConversionRate',
        //'conversion_type_name' => 'ConversionTypeName',
        'conversions' => 'Conversions',
        'converted_clicks' => 'ConvertedClicks',
        'cost' => 'Cost',
        //'cost_per_all_conversion' => 'CostPerAllConversion',
        'ctr' => 'Ctr',
        'date' => 'Date',
        'impressions' => 'Impressions',
        'search_impression_share' => 'SearchImpressionShare',
    ];

    protected $microAmounts = [
        'avg_cost',
        'avg_cpc',
        'cost',
        'cost_per_all_conversion'
    ];

    /**
     * Get the Report Definition Service of the current AdUser
     * @return ReportDefinitionService
     */
    protected function reportDefinitionService()
    {
        $result = $this->model->GetService("ReportDefinitionService", $this->adwords_version);
        return $result;
    }

    /**
     * Get a Report
     * @param App\Models\AdUser $aduser
     * @param int $customerId ID of the customer we're fetching a report for
     * @param array $fields Report fields to get
     * @param string $type Report Type to get
     * @param string $dateRangeType The type of date range to get. Defaults to YESTERDAY
     * @param array $dateRange Array containing our date range. Must contain 'min', and 'max'
     * 
     * @return array results
     */
    public function getReport(AdUser $aduser, $customerId, array $fields = null, $type = null, $dateRangeType = null, array $dateRange = null)
    {

        /*
         * Set our defaults if the params weren't passed.
         */
        if (is_null($fields)) {
            $fields = $this->reportFields;
        }
        if (is_null($dateRangeType)) {
            $dateRangeType = $this->dateRangeType;
        }
        if ($dateRangeType == 'CUSTOM_DATE' and is_null($dateRange)) {
            throw new Exception("Must specify date range to use CUSTOM_DATE", 1);
        }
        if (is_null($type)) {
            $type = $this->type;
        }

        $fields = collect($fields);

        $aduser->SetClientCustomerId($customerId);
        $aduser->LoadService('ReportDefinitionService', $this->adwords_version);

        // Create selector.
        $selector = new Selector();
        $selector->fields = $fields->values()->toArray();

        // Optional: use predicate to filter out paused criteria.
        //$selector->predicates[] = new Predicate('CampaignStatus', 'NOT_IN', array(
        //    'PAUSED'
        //));

        if ($dateRangeType == 'CUSTOM_DATE' and !is_null($dateRange)) {
            $selector->dateRange['min'] = $dateRange['min'];
            $selector->dateRange['max'] = $dateRange['max'];
        }
        
        // Create report definition.
        $reportDefinition = new ReportDefinition();
        $reportDefinition->selector = $selector;
        $reportDefinition->reportName = 'Report #' . uniqid();
        $reportDefinition->dateRangeType = $dateRangeType;
        $reportDefinition->reportType = $type;
        $reportDefinition->downloadFormat = 'CSV';

        // Set additional options.
        $options = [
            'version' => $this->adwords_version,
            'skipReportHeader' => true,
            'skipColumnHeader' => true,
            'includeZeroImpressions' => true
        ];

        // Download report.
        $report = ReportUtils::DownloadReport($reportDefinition, $filePath = null, $aduser, $options);

        // We'll read the report, and use the CSV library to turn it into an array.
        $reader = Reader::createFromString($report);
        $reportData = $reader->fetchOne(0);
        
        // The AdWords report uses different names than our database.
        // Combine our keys with their values.
        $results = collect(array_combine($fields->keys()->toArray(), $reportData))->toArray();

        return $results;
    }

}