<?php

namespace App\Traits;

use Selector;

trait CustomerService
{
    /**
     * The fields we select from accounts.
     *
     * @var array
     */
    protected $managedFields = [
        'CustomerId',
        'CompanyName',
        'Name',
        'CanManageClients',
        'TestAccount',
        'DateTimeZone'
    ];

    /**
     * Get the Customer Service of the current AdUser
     * @return CustomerService
     */
    public function customerService()
    {
        $result = $this->model->GetService("CustomerService")->get();
        return $result;
    }

    /**
     * Get the customer data
     *
     * Killing 2 birds with one stone here: 
     *     1 - Getting just the fields we need
     *     2 - Mapping the key names to our system
     *
     * @param $customerService Optional, if you don't pass a customer we will get the current
     * @return array
     */
    public function getCustomer(array $customerService = null)
    {
        if (is_null($customerService)) {
            $customerService = (array) $this->customerService();
        }
                                    
        return $this->mapCustomer($customerService);
    }

    /**
     * We're replacing the AdWords array keys with the keys we use
     * 
     * @param  array $customer
     * @return array $newCustomer
     */
    protected function mapCustomer(array $customer)
    {

        $customer = collect($customer);

        $newCustomer = collect([
                            'id' => '',
                            'date_time_zone' => '',
                            'name' => '',
                            'company_name' => '',
                            'descriptive_name' => '',
                            'can_manage' => '',
                            'test_account' => ''
                            ]);

        $replacements = collect([
                            'id' => 'customerId',
                            'date_time_zone' => 'dateTimeZone', 
                            'name' => 'name', 
                            'company_name' => 'companyName',
                            'can_manage' => 'canManageClients',
                            'test_account' => 'testAccount',
                            'descriptive_name' => 'descriptiveName' 
                            ]);

        $newCustomer->transform(function ($item, $key) use ($customer, $replacements) {
            
            $oldKey = $replacements->get($key);
            return $customer->get($oldKey);
        });

        return $newCustomer->toArray();
    }

    /**
     * Get the Managed Customer Service of the current AdUser
     * @return ManagedCustomerService
     */
    public function managedCustomerService()
    {
        // We have to set the customer ID before we can use
        // managed customer service.
        $customer = $this->customerService();
        $this->model->SetClientCustomerId($customer->customerId);

        $selector = new Selector();
        $selector->fields = $this->managedFields;

        // We'll pass the field selector to narrow it down
        $result = $this->model->GetService('ManagedCustomerService')->get($selector);
        //$this->resetAdUser();
        return $result;
    }

    /**
     * Get a collection containing the Managed Customers of the current AdUser
     * @return mixed
     */
    public function getManagedCustomers()
    {

        $managed = $this->managedCustomerService();

        if (isset($managed->entries)) // They have at least one managed account.
        {
            /*
             * $entries = Data object that represents a managed customer.
             * https://developers.google.com/adwords/api/docs/reference/v201506/ManagedCustomerService.ManagedCustomer
             *
             * $links = Represents an AdWords manager-client link.
             * https://developers.google.com/adwords/api/docs/reference/v201506/ManagedCustomerService.ManagedCustomerLink
             *
             * 
             * Entries has full info for all accounts AND the owner account,
             * Links has relationship info for just the clients
             *
             * So we'll sort through links, grab the ids, and reference their
             * full entries from the entries object
             */

            $links = collect($managed->links);
            $entries = collect($managed->entries);

            // Taking the links object down to just an array of Client IDs
            $links->transform(function ($link, $key) 
                {
                    return $link->clientCustomerId;
                });
            
            // Filtering the entries for only IDs of clients
            $customers = $entries->filter(function ($entry) use ($links) 
                {
                    return $links->contains($entry->customerId);
                });

            $customers->transform(function ($customer, $key) {

                $customer = (array) $customer;
                return $this->mapCustomer($customer);
                
            });

            return $customers->values();

        } else {
            return null;
        }

    }



    

}