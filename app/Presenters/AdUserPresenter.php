<?php

namespace App\Presenters;

use App\Transformers\AdUserTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AdUserPresenter
 *
 * @package namespace App\Presenters;
 */
class AdUserPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AdUserTransformer();
    }
}
