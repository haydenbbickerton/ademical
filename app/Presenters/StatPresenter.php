<?php

namespace App\Presenters;

use App\Transformers\StatTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AccountPresenter
 *
 * @package namespace App\Presenters;
 */
class StatPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new StatTransformer();
    }
}
