<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AccountRepository
 * @package namespace App\Contracts\Repositories;
 */
interface AccountRepository extends RepositoryInterface
{
    /**
     * Create a new account with the given data.
     *
     * @param  array  $data
     * @return App\Models\Account
     */
    public function create(array $data);

    /**
     * Create a new account with the given data.
     *
     * @param  App\Models\Account  $account
     * @param  \Google\Api\Ads\AdWords\Lib\AdWordsUser $aduser
     * @return void
     */
    public function updateManagedAccounts(array $clients, $manager);
}
