<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AccountRepository
 * @package namespace App\Contracts\Repositories;
 */
interface StatRepository extends RepositoryInterface
{
    /**
     * Create a new account with the given data.
     *
     * @param  array  $data
     * @return App\Models\Account
     */
    public function create(array $data);
}
