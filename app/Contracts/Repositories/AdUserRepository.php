<?php

namespace App\Contracts\Repositories;

/**
 * Interface AdUserRepository
 * @package namespace App\Contracts\Repositories;
 */
interface AdUserRepository
{

	/**
     * Initalize the AdUser from the given user.
     *
     * @param  $user An object containing user data
     * @return App\Models\AdUser
     */
    public function initAdUser($user);

	/**
     * Get the current AdUser.
     *
     * @return App\Models\AdUser
     */
    public function getAdUser();
}
