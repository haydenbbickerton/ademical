<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\AdUserRepository;
use App\Repositories\Eloquent\AccountRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Repositories\Eloquent\StatRepository;
use App\Contracts\Repositories\AccountRepository as AccountRepositoryContract;
use App\Contracts\Repositories\AdUserRepository as AdUserRepositoryContract;
use App\Contracts\Repositories\UserRepository as UserRepositoryContract;
use App\Contracts\Repositories\StatRepository as StatRepositoryContract;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->defineServices();
    }

    /**
     * Bind the services into the container.
     *
     * @return void
     */
    protected function defineServices()
    {
        $services = [
            AccountRepositoryContract::class => AccountRepository::class,
            AdUserRepositoryContract::class => AdUserRepository::class,
            UserRepositoryContract::class => UserRepository::class,
            StatRepositoryContract::class => StatRepository::class,
        ];

        foreach ($services as $key => $value) {
            $this->app->bindIf($key, $value);
        }
    }
}
